import numpy as np
from gaussian_method import *

def simplex(A, b, c, x_0=None, basis=None):
    """
    Solves linear program in standard form via simplex method with lexicographic pivoting.
    If no basic feasible solution as a starting point exists, the auxiliary problem is solved
    first to obtain a starting point.
    :param A: (m x n) array
    :param b: right handside
    :param c: objective function vector
    :param x_0: basic feasible solution
    :return: dictionary with solution solution, optimal cost, optimal basis, final tableau and transformations
    """

    (m, n) = A.shape

    if type(x_0) == type(None):
        dic = phase1(A, b, c)
        x_0 = dic['bfs']
        tableau = dic['tableau']
        basis = dic['basis']
        T = dic['T']
        T = np.eye(len(basis)+1)

    else:
        x_0 = np.asarray(x_0)

        if type(basis) == type(None):
            basis = [i for i in range(n) if np.round(x_0[i], 4) > 0]

        if len(basis) < m:
            raise ValueError("Can not infer basis since basic feasible solution is degenerate")

        A_basis = A[:, basis].copy()
        A_basis_inv = gauss(A_basis, x_0[basis])['T']

        # Add 0-th column
        tableau = np.c_[x_0[basis], A_basis_inv @ A]
        reduced_cost = c - c[basis] @ A_basis_inv @ A

        # add vector of reduced cost
        tableau = np.r_[[[- c[basis] @ x_0[basis]] + list(reduced_cost)], tableau]

        # Shift basis by +1 to fit the implementation of phase2
        basis = [el+1 for el in basis]
        T = np.eye(m+1)

    return phase2(tableau, basis, T)


def phase2(tableau, basis, T):
    """
    Solves the linear minimization problem c@x with constraints Ax=b, x>=0.
    :param A: mxn numpy array
    :param b: (m,) numpy array
    :param c: (n,) numpy array
    :return: dictionary with solution solution, optimal cost, optimal basis, final tableau and transformations
    """

    (m, n) = tableau.shape[0] - 1, tableau.shape[1] - 1

    # Start simplex method
    # Selected columns with negative reduced cost, as candidates for entering the basis.
    candidates = [index for index in range(1, n+1) if tableau[0, index] < 0 ]

    #print(f"Candidates : {candidates}")
    print(f"New basis : {basis}")

    iteration = 0

    while len(candidates) > 0:
        #pivot_column = candidates[np.argmin(tableau[0, candidates])]
        pivot_column = candidates[0]
        j = pivot_column
        positive_rows = [index for index in range(1, m+1) if tableau[index, j] > 0]
        print(f"Variable to enter the basis: {j} ")

        if len(positive_rows) == 0:
            raise ValueError('Cost is negative infinity')

        dummy_tableau = tableau.copy()
        for i in positive_rows:
            M = multiplication_matrix(i, 1/dummy_tableau[i, j], m+1)
            dummy_tableau = M@dummy_tableau

        # Choose lexicographically smallest row
        pivot_row = positive_rows[0]
        for i in positive_rows[1:]:
            pivot_row = lexico(dummy_tableau, pivot_row, i, n+1)

        print(f"Variable to leave the basis : {basis[pivot_row-1]}")

        M = multiplication_matrix(pivot_row, 1 / tableau[pivot_row, j], m + 1)
        tableau = M @ tableau
        T = M@T

        # Eliminate all non-zero entries in the pivot column except in the pivot row
        for i in range(m+1):
            if i == pivot_row or tableau[i, j] == 0:
                continue

            else:
                G = subtraction_matrix(i, pivot_row, tableau[i, j]/tableau[pivot_row, j], m+1)
                tableau = G@tableau
                T = G@T

        # Update basis
        basis[pivot_row-1] = pivot_column

        # Get new columns with negative reduced cost
        candidates = [index for index in range(1, n + 1) if tableau[0, index] < 0]
        iteration += 1
        print(f"New basis : {basis}")
        print(f"Iteration: {iteration}")

    x = np.zeros(n)
    for i in range(m):
        x[basis[i]-1] = tableau[i+1, 0]

    for el in x:
        if el < 0:
            raise ValueError('Solution is infeasible')

    return {"tableau" : tableau, "cost" : -tableau[0, 0], "basis" : basis, 'solution' : x, 'T' : T}










def lexico(tableau, aindex, bindex, ncols):
    """
    Returns lexicographically smallest row. If aindex = bindex then returns a.
    :param tableau: simplex tableau
    :param aindex: index of row
    :param bindex: index of another row
    :param ncols: number of columns in tableau
    :return: index of lexicographically smaller row.
    """
    for j in range(ncols):
        if tableau[aindex, j] == tableau[bindex, j]:
            continue
        else:
            if tableau[aindex, j] < tableau[bindex, j]:
                return aindex
            else:
                return bindex

    return aindex


def phase1(A, b, c):
    """
    Solves auxiliary problem by adding a basis of the column space and weight vector only penalizing
    the new columns. Also drops linearly dependent rows.
    :param A: m x n array
    :param b: array of length m
    :param c: array of length n
    :return: dictionary of basic feasible solution, final tableau, basis and transformations
    """
    (m, n) = A.shape

    for i in range(m):
        if b[i] < 0:
            b[i] *= -1
            A[i, :] *= -1

    # Make tableau for auxiliary problem
    tableau = np.c_[b, A, np.eye(m)]
    c_art = np.zeros(n+m+1)
    for i in range(m):
        c_art[n + i +1] = 1

    tableau = np.r_[[c_art], tableau]
    T = np.eye(m+1)

    # Compute reduced costs
    for i in range(m):
        G = subtraction_matrix(0, i+1, 1, m+1)
        tableau = G@tableau
        T = G@T

    basis = list(range(n+1, n+m+1))

    dic = phase2(tableau, basis, T)

    if np.round(dic['cost'], 4) != 0:
        raise ValueError('Problem is infeasible')

    keep_rows = list(range(m+1))
    old_basis = basis.copy()
    for i in range(m):
        if (dic['tableau'][i+1, 1:n+1].round(4) == 0).all():
            keep_rows.remove(i+1)
            basis.remove(old_basis[i])

    # Drop linearly dependent rows
    dic['tableau'] = dic['tableau'][keep_rows]

    # Drop artificial columns
    dic['tableau'] = dic['tableau'][:, :n+1]

    m = dic['tableau'].shape[0] - 1
    n = dic['tableau'].shape[1] - 1

    # Compute cost and reduced cost for original cost vector c
    dic['tableau'][0][0] = - c[[el-1 for el in basis]]@dic['tableau'][1:, 0]

    for j in range(1, n+1):
        if not j in basis:
            dic['tableau'][0, j] = c[j-1] - c[[el-1 for el in basis]]@dic['tableau'][1:, j]

    x = np.zeros(n)
    for i in range(m):
        x[basis[i]-1] = dic['tableau'][i+1, 0]

    return {'bfs' : x, 'tableau' : dic['tableau'], 'basis' : dic['basis'], 'T' : dic['T']}
