import numpy as np

def gauss(matrix, right_hand_side, decimals=3):
    """
    Takes in matrix and right hand side, outputs basic feasible solution for Ax=b, basis,
    rank and transformed augmented matrix.
    :param matrix:
    :param right_hand_side:
    :param decimals:
    :return: dictionary of basic feasible solution, basis,
    rank and transformed augmented matrix.
    """
    m, n = np.shape(matrix)
    A = np.c_[matrix, right_hand_side]
    basis = []

    T = np.eye(m)
    pivot = 0
    for j in range(n):

        # Find rows with non-zero entries in or under pivot rows
        tmp = None
        for i in range(pivot, m):
            if A[i, j] != 0:
                pivot_row = i
                P = row_permutation_matrix(pivot, pivot_row, m)
                T = P@T
                A = P@A
                break
            tmp = i

        if tmp == m-1:
            continue
        # Normalize pivot
        else:
            M = multiplication_matrix(pivot, 1/A[pivot, j], m)
            T = M@T
            A = M@A
            basis.append(j)

        # Eliminate non-zero entries
        for i in range(m):
            if A[i, j] == 0 or i == pivot:
                continue

            else:
                G = subtraction_matrix(i, pivot, A[i, j], m)
                T = G@T
                A = G@A

        pivot += 1

        if pivot == m:
            break


    rank = len(basis)
    solution = np.zeros(n)
    for i in range(rank):
        solution[basis[i]] = A[i,-1]

    return {'bfs' : solution, 'basis' : basis, 'rank' : rank, 'tableau' : A.round(decimals), 'T' : T}


def row_permutation_matrix(i, j, m):
    P = np.eye(m, dtype="int64")
    e_i = np.zeros(m)
    e_i[i] = 1
    e_j = np.zeros(m)
    e_j[j] = 1
    P[i, :] = e_j
    P[j, :] = e_i

    return P

def subtraction_matrix(i, pivot, alpha, m):
    """
    Returns matrix which when left-multiplicated to another Matrix subtracts alpha times the pivot-th from the i-th row, i.e.
    row i := row i - alphja * pivot row.
    :param pivot: Row to be minuend
    :param i: Row to be subtrahend
    :param alpha: Multiplied by pivot-th row
    :param m: number of rows and columns of matrix
    :return: matrix G
    """

    G = np.eye(m)
    difference_row = m*[0]
    difference_row[i] = 1
    difference_row[pivot] = -alpha
    G[i, :] = difference_row

    return G

def multiplication_matrix(i, alpha, m):
    """
    Returns matrix which when left-multiplied to another matrix multiplies the i-th row by alpha.
    :param alpha: number
    :param m: number of rows and columns
    :return: matrix M
    """

    M = np.eye(m)
    M[i][i] = alpha

    return M